# When sourced by a script, makes it exit on various errors, print a call stack
# and provides a way to run commands without triggering the exit mechanism

set -eEuo pipefail

trap "print_callers" ERR

print_callers() {
  echo "Got an error. Call stack:"
  i=0; while caller $i; do let i=i+1; done
}

# run_allowerr <ALLOWED_EXIT_CODES> <CMD>
# ALLOWED_EXIT_CODES is a space-separated list. 0 is allowed implicitly
# CMD is a string passed to 'eval'
run_allowerr() {
  local ALLOWED_EXIT_CODES="0 $1"
  local CMD="$2"

  set +e
  trap - ERR

  OUTPUT="$(eval "$CMD" 2>&1)"
  EXIT_CODE=$?

  set -e
  trap "print_callers" ERR

  for NUM in $ALLOWED_EXIT_CODES; do
    if [ $EXIT_CODE -eq $NUM ]; then return 0; fi
  done

  echo "exit code '$EXIT_CODE' not allowed for '$CMD'. Output: $OUTPUT"
  return $EXIT_CODE
}
