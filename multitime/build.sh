#!/bin/sh

cp lib/multitime.sh .

cat << "EOF" >> multitime.sh
set -euo pipefail

parse_args "$@"

run
EOF
