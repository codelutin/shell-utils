Include lib/multitime.sh

Describe 'Test check_syntax()'
	It 'calls check_syntax()'
		When call check_syntax "--test" "ls -l"
		The status should be success
	End

	It 'calls check_syntax() with bad command'
		When call check_syntax "--test" "ls -l ("
		The status should be failure
		The stderr should include "Syntax error on --test"
		The stderr should include "syntax error near unexpected token \`('"
	End
End

Describe 'Test get_stats()'
	real_a=(1,075 0,921 0,718)
	expected_output() { %text
		#|min = 0.718
		#|max = 1.075
		#|mean = 0.904667
	}
	It 'calls get_stats()'
		When call get_stats real_a
		The status should be success
		The output should equal "$(expected_output)"
	End
End
