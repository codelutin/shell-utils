# multitime.sh

A wrapper around Bash 'time' builtin, useful to run a command multiple times and print some stats. This is similar to the more complete [multitime](https://tratt.net/laurie/src/multitime/), but does not require installation and should run on most distros (including Alpine, if Bash is installed)
