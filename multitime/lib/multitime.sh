#!/bin/bash

# err <msg>
err() {
	echo -e "\033[1;31mError: $1\033[0m" >&2
	exit 1
}

# warn <msg>
warn() {
	echo -e "\033[1;33mWarning: $1\033[0m" >&2
}

# check_syntax <command_name> <command>
# Performs a basic syntax check
check_syntax() (
	set +e
	local COMMAND_NAME="$1"
	local COMMAND="$2"
	local SYNTAX_CHECK_RESULT
	SYNTAX_CHECK_RESULT=$(echo "$COMMAND" | bash -n 2>&1) # Assignment in a separate line in order to capture return code
	[ $? -ne 0 ] && err "Syntax error on $COMMAND_NAME:\n$SYNTAX_CHECK_RESULT"
	return 0
)

get_stats() {
	local -n ARRAY=$1
	NUMBERS=$(printf '%s\n' "${ARRAY[@]//,/.}")
	echo "$NUMBERS" | awk 'NR==1||$0<x{x=$0}END{print "min = " x}'
	echo "$NUMBERS" | awk 'NR==1||$0>x{x=$0}END{print "max = " x}'
	echo "$NUMBERS" | awk '{x+=$0}END{print "mean = " x/NR}'
}

run() {
	export TIMEFORMAT='%R %U %S'

	real_a=()
	user_a=()
	system_a=()

	for i in $(seq "$NUMRUNS"); do
		$BEFORE_COMMAND

		# The 'time' used will be the Bash builtin. All output from the command will
		# be discarded, but the 'time' output will still be captured as it runs in
		# a subshell
		read real user system < <( { time $COMMAND &> /dev/null ; } 2>&1 )

		$AFTER_COMMAND

		real_a+=($real)
		user_a+=($user)
		system_a+=($system)
	done

	echo "Real:"
	get_stats real_a
	echo -e "\nUser:"
	get_stats user_a
	echo -e "\nSystem:"
	get_stats system_a
}

USAGE="Usage: $0 [-h | --help] [[-n | --numruns] <numruns>] [--before-command <command>] [--after-command <command>] --command <command>"
# usage
usage() { echo "$USAGE"; exit 0; }
# warnusage <message>
warnusage() { warn "$1"; echo "$USAGE" >&2; exit 1; }

# parse_args
parse_args() {
	# Default values
	NUMRUNS=1
	BEFORE_COMMAND=""
	AFTER_COMMAND=""
	COMMAND=""

	local CURRENT_ARG

	while [ $# -gt 0 ]; do
		CURRENT_ARG="$1"

		case ${CURRENT_ARG} in
			-h | --help)
				usage
				;;
			-n | --numruns)
				NUMRUNS="$2"
				[[ "$NUMRUNS" =~ ^[0-9]+$ ]] || err "$NUMRUNS is not a positive integer"
				shift
				;;
			--before-command)
				BEFORE_COMMAND="$2"
				check_syntax "--before-command" "$BEFORE_COMMAND"
				shift
				;;
			--before-command)
				BEFORE_COMMAND="$2"
				check_syntax "--before-command" "$BEFORE_COMMAND"
				shift
				;;
			--after-command)
				AFTER_COMMAND="$2"
				check_syntax "--after-command" "$BAFTER_COMMAND"
				shift
				;;
			--command)
				COMMAND="$2"
				check_syntax "--command" "$COMMAND"
				shift
				;;
			-*)
				warnusage "${CURRENT_ARG}: unknown option"
				;;
		esac
		shift
	done

	if [ -z "$COMMAND" ]; then
		warnusage "--command is required"
	fi
}
